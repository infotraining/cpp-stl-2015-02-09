#include "person.hpp"
#include "utils.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using namespace std;

class CheckSalary : public binary_function<Person, int, bool>
{
public:
	bool operator()(const Person& p, int threshold) const
	{
		return p.salary() < threshold;
	}
};

class CheckAge : public binary_function<Person, int, bool>
{
public:
	bool operator()(const Person& p, unsigned int threshold) const
	{
		return p.age() < threshold;
	}
};

class CompareByName : public binary_function<Person, Person, bool>
{
public:
	bool operator()(const Person& p1, const Person& p2) const
	{
		return p1.name() < p2.name();
	}
};

class GenderSelector : public unary_function<Person, bool>
{
	Gender gender_;
public:
	GenderSelector(Gender g) : gender_(g)
	{
	}

	bool operator()(const Person& p)
	{
		return p.gender() == gender_;
	}
};

class AvgSalaryCounter
{
	double sum_;
	size_t count_;
public:
	AvgSalaryCounter() : sum_(0), count_(0) {}

	void operator()(const Person& e)
	{
		sum_ += e.salary();
		count_++;
	}

	double get_avg_salary()
	{
		return sum_ / count_;
	}
};

int main()
{
	vector<Person> employees;
	fill_person_container(employees);

	cout << "Wszyscy pracownicy:\n";
	copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
	cout << endl;

	// wyswietl pracownikow z pensja powyzej 3000
	cout << "\nPracownicy z pensja powy�ej 3000:\n";
	remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"), 
		bind2nd(CheckSalary(), 3000));

	// wy�wietl pracownik�w o wieku poni�ej 30 lat
	cout << "\nPracownicy o wieku poni�ej 30 lat:\n";
	remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"), 
		not1(bind2nd(CheckAge(), 30)));

	// posortuj malej�co pracownikow wg nazwiska
	cout << "\nLista pracownik�w wg nazwiska (malejaco):\n";
	sort(employees.begin(), employees.end(), not2(CompareByName()));
	copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

	// wy�wietl kobiety
	cout << "\nKobiety:\n";
	remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"), 
		GenderSelector(Male));

	// ilosc osob zarabiajacych powyzej sredniej
	cout << "\nIlosc osob zarabiajacych powyzej sredniej:\n";
	AvgSalaryCounter asc = for_each(employees.begin(), employees.end(), AvgSalaryCounter());
	cout << "Srednia pensja: " << asc.get_avg_salary() << endl;
	remove_copy_if(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"), 
		bind2nd(CheckSalary(), asc.get_avg_salary()));
}