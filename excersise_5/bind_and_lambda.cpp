#include "person.hpp"

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <functional>

using namespace std;

int main()
{
    vector<Person> employees;
    fill_person_container(employees);

    cout << "Wszyscy pracownicy:\n";
    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));
    cout << endl;

    // wyświetl pracownikow z pensją powyżej 3000
    cout << "Over 3000 salary" << endl;
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            [](const Person& p) { return p.salary() > 3000; });

    // wyświetl pracowników o wieku poniżej 30 lat
    cout << "Under 30" << endl;

    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            [](const Person& p) { return p.age() < 30; });

    // posortuj malejąco pracownikow wg nazwiska

    cout << "Sorted by name" << endl;
    sort(employees.begin(), employees.end(),
         [] (const Person& p1, const Person& p2) { return p1.name() > p2.name();});

    copy(employees.begin(), employees.end(), ostream_iterator<Person>(cout, "\n"));

    // wyświetl kobiety
    cout << "Sorted by gender" << endl;
    copy_if(employees.begin(), employees.end(),
            ostream_iterator<Person>(cout, "\n"),
            [](const Person& p) { return p.gender() == Gender::female; });


    // ilość osob zarabiajacych powyżej średniej
    double avg = accumulate(employees.begin(), employees.end(), 0.0,
                            [] (double d, const Person& p) { return d + p.salary();});

    avg = avg / employees.size();

    cout << "above avg salary = ";
    cout << count_if(employees.begin(), employees.end(),
            [avg](const Person& p) { return p.salary() > avg;});
    cout << endl;








}
