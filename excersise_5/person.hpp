#ifndef PERSON_HPP
#define PERSON_HPP

#include <string>
#include <iostream>
#include <vector>

enum class Gender { male, female };

class Person
{
	std::string name_;
	unsigned int age_;
	double salary_;
	Gender gender_;
public:
	Person(const std::string& name, unsigned int age, double salary, Gender gender);

	std::string name() const;
	void set_name(const std::string& name);

	unsigned int age() const;
	void set_age(unsigned int age);

	double salary() const;
	void set_salary(double salary);

	Gender gender() const;
	void set_gender(Gender gender);

	void print() const;
};

std::ostream& operator <<(std::ostream& out, const Person& p);

template <typename Container>
void fill_person_container(Container& container)
{
    container.emplace_back("Gruszka", 45, 3000.0, Gender::male);
    container.emplace_back("Malinowska", 33, 5500.0, Gender::female);
    container.emplace_back("Brzozowski", 28, 2000.0, Gender::male);
    container.emplace_back("Wierzbowski", 54, 7900.0, Gender::male);
    container.emplace_back("Kowalski", 44, 4300.0, Gender::male);
    container.emplace_back("Kowalewska", 23, 2450.0, Gender::female);
    container.emplace_back("Nowak", 58, 4900.0, Gender::male);
    container.emplace_back("Kowal", 29, 3700.0, Gender::male);
    container.emplace_back("Zawadzka", 64, 5300.0, Gender::female);
    container.emplace_back("Hoffander", 53, 7600.0, Gender::male);
    container.emplace_back("Paluch", 53, 2000.0, Gender::male);
    container.emplace_back("Pruski", 41, 9000.0, Gender::male);
};

#endif
