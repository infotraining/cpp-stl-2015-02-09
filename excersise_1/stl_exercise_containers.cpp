#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <cassert>
#include <list>
#include <functional>
#include <iterator>

using namespace std;

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}


void avg(int array[], size_t size)
{
    double sum = 0.0;
    for(size_t i = 0; i < size; ++i)
        sum += array[i];

    cout << "AVG: " << sum / size << endl;
}

bool is_even(int n)
{
    return n % 2 == 0;
}

int main()
{
    int tab[100];

    for(int i = 0; i < 100; ++i)
        tab[i] = rand() % 100;

    cout << "tab: ";
    for(int i = 0; i < 100; ++i)
        cout << tab[i] << " ";
    cout << "\n\n";

    // 1 - utwórz wektor vec_int zawierający kopie wszystkich elementów z  tablicy tab
    vector<int> vec_int(begin(tab), end(tab)); // C++11
    //vector<int> vec_int(tab, tab+100);
    print(vec_int);

    // 2 - wypisz wartość średnią przechowywanych liczb w vec_int
    avg(vec_int.data(), vec_int.size());

    // 3a - utwórz kopię wektora vec_int o nazwie vec_cloned
    vector<int> vec_cloned(vec_int);
    print(vec_cloned);

    // 3b - wyczysć kontener vec_int i zwolnij pamięć po buforze wektora
    vec_int.clear();
    vec_int.shrink_to_fit();
    cout << "capacity after clear: " << vec_int.capacity() << endl;

    int rest[] = {1,2,3,4};
    // 4 - dodaj na koniec wektora vec_cloned zawartość tablicy rest
    for(auto& el : rest)
        vec_cloned.push_back(el);
    // or
    vec_cloned.insert(vec_cloned.end(),
                      begin(rest), end(rest));
    cout << "cap: " << vec_cloned.capacity() << endl;
    print(vec_cloned);

    // 5 - posortuj zawartość wektora vec_cloned
    sort(begin(vec_cloned), end(vec_cloned));
    print(vec_cloned);

    // 6 - wyświetl na ekranie zawartość vec_cloned za pomocą iteratorów
    for(vector<int>::const_iterator it = vec_cloned.begin();
        it != vec_cloned.end() ; ++it)
    {
        cout << *it << ", ";
    }
    cout << "--------------------------" <<  endl;

    // 7 - utwórz kontener numbers (wybierz odpowiedni typ biorąc pod uwagę następne polecenia) zawierający kopie elementów vec_cloned
    list<int> numbers(begin(vec_cloned), end(vec_cloned));
    print(numbers);

    // 8 - usuń duplikaty elementów przechowywanych w kontenerze

    numbers.unique();
    print(numbers);

    // 9 - usuń liczby parzyste z kontenera
    //numbers.remove_if(&is_even);

    numbers.remove_if( [] (int n) { return !(n % 2);} );
    print(numbers);
    // 10 - wyświetl elementy kontenera w odwrotnej kolejności

    // 11 - skopiuj dane z numbers do listy lst_numbers jednocześnie i wyczyść kontener numbers
    list<int> lst_numbers(move(numbers));

    // method 1
    //list<int> lst_numbers;
    //swap(lst_numbers, numbers);
    print(lst_numbers);
    print(numbers);

    // 12 - wyświetl elementy kontenera lst_numbers w odwrotnej kolejności
    for (auto it = lst_numbers.rbegin() ; it != lst_numbers.rend() ; ++it)
    {
        cout << *it << " - ";
    }
    cout << endl;
}

