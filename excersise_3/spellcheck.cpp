#include <iostream>
#include <fstream>
#include <set>
#include <unordered_set>
#include <string>
#include <chrono>
#include <unordered_set>
#include <iterator>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <sstream>

using namespace std;

int main()
{
     // wczytaj zawartość pliku en.dict ("słownik języka angielskieog")
     // sprawdź poprawość pisowni następującego zdania:

    //string input_text = "this is an exple of snetence";
    string input_text = "to jest bledne zdanie przyklad";
    vector<string> words;
    string word;
    //boost::split(words, input_text, boost::is_any_of(" "));

    // preparing dictionary

    auto start = std::chrono::high_resolution_clock::now();

    ifstream in_dict("/home/leszek/_code_/excersise_3/pl.dict");
    if (!in_dict)
        cerr << "Error opening file" << endl;

    unordered_set<string> dict;
    //set<string> dict;
    //vector<string> dict;

    //statistics
    dict.reserve(5098259);
    cout << "Bucket count: " << dict.bucket_count() << endl;
    cout << "Load factor: " << dict.load_factor() << endl;


    while(in_dict >> word)
        dict.insert(word);
        //dict.push_back(word);

    //sort(dict.begin(), dict.end());

    auto end = std::chrono::high_resolution_clock::now();
    cout << "Loading: "
         <<  std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    //dict.rehash(5098259);
    cout << "Bucket count: " << dict.bucket_count() << endl;
    cout << "Load factor: " << dict.load_factor() << endl;


    // preparing input text

    istringstream str(input_text);

    while(str >> word)
        words.push_back(word);

    // checking

    start = std::chrono::high_resolution_clock::now();

    for (auto& word : words)
        //if (binary_search(dict.begin(), dict.end(), word) == false)
        if (dict.count(word) == 0)
            cout << word << ", ";

    end = std::chrono::high_resolution_clock::now();
    cout << "Checking: "
         <<  std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count()
         << " ns" << endl;


    cout << endl;
}

