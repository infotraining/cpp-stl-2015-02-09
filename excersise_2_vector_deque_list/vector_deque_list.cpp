#include <iostream>
#include <vector>
#include <deque>
#include <list>
#include <ctime>
#include <algorithm>
#include <cstring>
#include <chrono>
#include <random>

using namespace std;

// szablony funkcji umożliwiające sortowanie kontenera
class ExpensiveObject
{
private:
    char* data_;
    size_t size_;
public:
    ExpensiveObject(const char* data) : data_(NULL), size_(std::strlen(data))
    {
        data_ = new char[size_+1];
        std::strcpy(data_, data);
    }

    ExpensiveObject(const ExpensiveObject& source) : data_(NULL), size_(source.size_)
    {
        data_ = new char[size_+1];
        std::strcpy(data_, source.data_);
    }

    ExpensiveObject(ExpensiveObject&& source) noexcept
        : size_(source.size_)
    {
        data_ = source.data_;
        source.data_ = nullptr;
    }

    ~ExpensiveObject()
    {
        delete [] data_;
    }

    ExpensiveObject& operator=(const ExpensiveObject& source)
    {
        ExpensiveObject temp(source);
        swap(temp);
        return *this;
    }

    ExpensiveObject& operator=(ExpensiveObject&& source) noexcept
    {
        delete [] data_;
        data_ = source.data_;
        size_ = source.size_;
        source.data_ = nullptr;
        return *this;
    }

    void swap(ExpensiveObject& other)
    {
        std::swap(data_, other.data_);
        std::swap(size_, other.size_);
    }

    const char* data() const
    {
        return data_;
    }

    size_t size() const
    {
        return size_;
    }

    bool operator<(const ExpensiveObject& other) const
    {
        if (std::strcmp(data_, other.data_) < 0)
            return true;
        return false;
    }
};

ExpensiveObject expensive_object_generator()
{
    static char txt[] = "abcdefghijklmn";
    static const size_t size = std::strlen(txt);

    std::random_shuffle(txt, txt + size);

    return ExpensiveObject(txt);
}

template<typename Cont>
void sort(Cont& cont)
{
    sort(std::begin(cont), std::end(cont));
}

template<typename T>
void sort(list<T>& cont)
{
    cont.sort();
}

template<typename Cont>
void test(Cont cont, size_t size)
{
    std::random_device rd;
    std::mt19937_64 gen(rd());
    std::uniform_int_distribution<> dist(0,100);

    auto start = std::chrono::high_resolution_clock::now();

    for (size_t i = 0 ; i < size ; ++i)
        cont.push_back(expensive_object_generator());
    auto end = std::chrono::high_resolution_clock::now();

    cout << "Filling: "
         <<  std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    start = std::chrono::high_resolution_clock::now();
    sort(cont);
    end = std::chrono::high_resolution_clock::now();
    cout << "Sorting: "
         << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count()
         << " ms" << endl;
}

int main()
{
/*
    Utwórz trzy sekwencje typu int: vector, deque i list. Wypelnij je wartosciami losowymi.
    Porównaj czasy tworzenia i wypelniania danymi sekwencji. Napisz szablon funkcji sortującej sekwencje
    vector i deque. Napisz specjalizowany szablon funkcji realizującej sortowanie dla kontenera list.
    Porównaj efektywność operacji sortowania.
*/
    for (int i = 10000 ; i <= 1000000 ; i *= 10)
    {
        vector<ExpensiveObject> v;
        cout << "Vector size " << i << endl;
        test(v, i);
        cout << "Deque size " << i << endl;
        deque<ExpensiveObject> d;
        test(d, i);
        cout << "List size " << i << endl;
        list<ExpensiveObject> l;
        test(l, i);

        cout << "---------" << endl;
    }


}
