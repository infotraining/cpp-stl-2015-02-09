#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
#include <functional>
#include <random>
#include <iterator>

using namespace std;

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

int main()
{
    vector<int> vec(30);
    random_device rd;
    mt19937_64 mt(rd());
    uniform_int_distribution<> dist(1, 20);

    generate(vec.begin(), vec.end(), [&] () { return dist(mt);});
    print(vec);

    /*int i = 0 ;
    auto g = [&i] () { return i;};
    for (; i < 30 ; ++i)
    {
        vec[i] = g();
    }
    print(vec);*/

    //int limit = *vec.begin();
    copy_if(vec.begin(), vec.end(), ostream_iterator<int>(cout, ", "),
            [&vec] (int a) { return a < *vec.begin() ;});
    cout << endl;


    return 0;
}

