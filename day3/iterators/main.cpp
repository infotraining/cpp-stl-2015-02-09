#include <iostream>
#include <vector>
#include <iterator>
#include <string>
#include <list>

using namespace std;

int main()
{
    cout << "Iterators" << endl;
    vector<int> v1{3,4,5,6};
    vector<int> v2{1,2,3,4,5};

    back_insert_iterator<vector<int>> bit(v1);

    bit = 10;
    bit++;
//    bit = 20;
//    bit = 30;

    copy(v2.begin(), v2.end(), bit);

    // input iterators


    istream_iterator<string> start(cin);
    istream_iterator<string> end;

    vector<string> vstr(start, end);

    for (auto& el : vstr)
        cout << el << ", ";
    cout << endl;

    copy(vstr.begin(), vstr.end(), ostream_iterator<string>(cout, ": "));
    cout << endl;


    cout << "Vec: ";
    for (auto& el : v1)
        cout << el << ", ";
    cout << endl;

    cout << "first five elements: ";
    copy(v1.begin(), v1.begin()+50, ostream_iterator<int>(cout, ", "));
    cout << endl;

    // move iterators

    list<string> s{"one", "two", "three"};

    vector<string> vs1(s.begin(), s.end()); // copy

    cout << "\noriginal list after copy now holds: ";
    for (auto str : s)
            cout << "\"" << str << "\" ";
    cout << '\n';

    vector<string> vs2(make_move_iterator(s.begin()),
                                make_move_iterator(s.end())); // move

    cout << "v1 now holds: ";
    for (auto str : vs1)
            cout << "\"" << str << "\" ";
    cout << "\nv2 now holds: ";
    for (auto str : vs2)
            cout << "\"" << str << "\" ";
    cout << "\noriginal list now holds: ";
    for (auto str : s)
            cout << "\"" << str << "\" ";
    cout << '\n';



    return 0;
}

