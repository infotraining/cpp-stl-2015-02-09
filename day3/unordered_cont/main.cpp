#include <iostream>
#include <unordered_set>
#include <set>
#include <random>
#include <chrono>
#include <boost/functional/hash.hpp>

using namespace std;

struct Point
{
    int x, y;
    Point(int x, int y) : x(x), y(y) {}

//    bool operator <(const Point& other) const
//    {
//        return (x+y) < (other.x + other.y);
//    }

    bool operator ==(const Point& other) const
    {
        return (x == other.x) && (y == other.y);
    }
};

namespace std {

    template<>
    struct hash<Point>
    {
        size_t operator()(const Point& p) const
        {
            std::hash<int> hasher;
            size_t seed{};
            return hasher(p.x + p.y*100);
            //return hasher(p.x);
            //boost::hash_combine(seed, p.x);
            //boost::hash_combine(seed, p.y);
            return seed;
        }
    };
}

int main()
{

    unordered_set<Point> sp;
    sp.reserve(1000000);
    random_device rd;
    mt19937_64 gen(rd());
    uniform_int_distribution<> dist(0,100000);

    // creating points

    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0 ; i < 1000000 ; ++i)
        sp.emplace(dist(gen), dist(gen));

    auto end = std::chrono::high_resolution_clock::now();
    cout << "Creating: "
         <<  std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    cout << "Bucket count: " << sp.bucket_count() << endl;
    cout << "Load factor: " << sp.load_factor() << endl;

    // checking points

    start = std::chrono::high_resolution_clock::now();

    long hits = 0;
    for (int i = 0 ; i < 1000000 ; ++i)
        if (sp.count(Point(dist(gen), dist(gen))) != 0)
            ++hits;

    end = std::chrono::high_resolution_clock::now();
    cout << "Checking: "
         <<  std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    cout << "Hits = " << hits << endl;
    return 0;
}


