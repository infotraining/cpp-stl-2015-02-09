#include <iostream>
#include <map>
#include <fstream>
#include <string>
#include <boost/algorithm/string.hpp>
#include <functional>
#include <chrono>
#include <unordered_map>

using namespace std;

int main()
{
    cout << "Concordance for Swans's way by Marcel Proust" << endl;

    ifstream inpf("/home/leszek/swan.txt");
    if(!inpf) cerr << "Error opening file" << endl;;

    auto start = std::chrono::high_resolution_clock::now();

    unordered_map<string, int> counter;
    string word;
    while(inpf >> word)
    {
        boost::to_lower(word);
        boost::trim_if(word, boost::is_any_of("\t !:;?.(),-\""));
        counter[move(word)] += 1;
    }

    map<int, vector<string>, greater<int>> result;
    for (auto& el : counter)
    {
         result[el.second].emplace_back(el.first);
    }

    /*
    vector<unique_ptr<Gadget>> v;
    unique_ptr<Gadget> p(new Gadget(1, "ala") ;
    v.push_back(move(p));
    v.emplace_back(new Gadget(1, "ala"));
    v.push_back(make_unique<Gadget>(1, "ala"));

    */
    /*
    vector<thread> thds;
    thread th1(my_fun, 1, 2 ,3);
    thds.push_back(move(th1)); // good

    thds.push_back(thread(my_fun, 1,2,3)); // better
    thds.emplace_back(my_fun, 1,2,3); // best
    */

    auto end = std::chrono::high_resolution_clock::now();
    cout << "Total: "
         <<  std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count()
         << " ms" << endl;

    int c{};
    for (auto& el : result) // display
    {
        if (c > 10) break;

        cout << el.first << " ";
        for (auto& word : el.second)
            cout << word << ", " ;
        cout << endl;
        ++c;
    }

    return 0;
}

