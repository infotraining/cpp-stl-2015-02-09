#include <iostream>
#include <vector>

using namespace std;

int main()
{
    cout << "Hello bool vector!" << endl;
    vector<bool> v;

    cout << "Sizeof bool " << sizeof(bool) << endl;
    v.push_back(true);
    v.push_back(false);
    //v.flip();

    for(bool&& el : v)
        cout << el << ", ";
    cout << endl;
    return 0;
}

