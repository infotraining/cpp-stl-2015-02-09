#include <iostream>
#include <map>

using namespace std;

int main()
{
    map<string, double> stocks;

    stocks.insert(pair<string, double>("APPL", 12.34));
    stocks.insert(make_pair("GOGL", 22.34));

    // counter intuitive?
    stocks.insert(make_pair("GOGL", 122.34)); // non modyfying

    stocks["NKIA"] = 1234.56;
    stocks["NKIA"] = 12340.56;

    cout << "Apple price: " << stocks["APPL"] << endl; // read

    // counter intuitive?
    cout << "Aplle price: " << stocks["APLL"] << endl; // creating


    for(auto& el : stocks)
    {
        cout << el.first << ", " << el.second << endl;
    }

    return 0;
}

