#include <iostream>
#include <list>
//#include <forward_list>

using namespace std;

int main()
{
    cout << "Hello list" << endl;
    list<int> l{2, 2, 1, 3, 4, 2, 3, 5, 4};

    l.sort();
    l.unique();
    for (auto& el: l)
        cout << el << ", ";
    cout << endl;
    list<int> l2{10, 10, 20, 30};
    l.merge(l2);
    l.remove(10);
    for (auto& el: l)
        cout << el << ", ";
    cout << endl;
    return 0;

}

