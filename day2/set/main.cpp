#include <iostream>
#include <set>
#include <functional>

using namespace std;

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_(id)
    {
        cout << "ctor " << id_ << endl;
    }
    // -- automatic... //
    ~Gadget()
    {
        cout << "dtor" << endl;
    }

    Gadget(const Gadget& other) = delete;
//    {
//        id_ = other.id_;
//        cout << "cpy ctor " << id_ << endl;
//    }

    Gadget operator=(const Gadget& other) = delete;
//    {
//        id_ = other.id_;
//        cout << "cpy = " << endl;
//        return *this;
//    }
//    // move semantic C++11 and up
    Gadget(Gadget&& rother) noexcept
    {
        swap(id_, rother.id_);
        cout << "move ctor " << id_ << endl;
    }

    Gadget operator=(Gadget&& rother) noexcept
    {
        swap(id_, rother.id_);
        cout << "move =" << endl;
        return move(*this);
    }
    int id() const
    {
        return id_;
    }
};


int main()
{
    cout << "Hello Set" << endl;
    set<int, greater<int>> s;

    if (s.insert(4).second == true)
        cout << "4 is inserted" << endl;
    else
        cout << "4 was already in set" << endl;

    if (s.insert(4).second == true)
        cout << "4 is inserted" << endl;
    else
        cout << "4 was already in set" << endl;
    s.insert(2);
    s.insert(11);

    cout << "Czy jest 4 " << s.count(4) << endl;
    cout << "Czy jest 5 " << s.count(5) << endl;
    for (auto& el : s)
        cout << el << ", " << endl;

    // set of Gadgets
    cout << "----------------------------" << endl;
    set<Gadget, std::function<bool(const Gadget&, const Gadget&)>>
          sg([](const Gadget& g1, const Gadget& g2) { return g1.id() < g2.id(); });
    sg.emplace(3);
    sg.emplace(1);
    sg.emplace(2);
    for (auto& el : sg)
        cout << el.id() << ", " << endl;

    cout << "----------------------------" << endl;

    auto comp = [](const Gadget& g1, const Gadget& g2) { return g1.id() > g2.id(); };
    set<Gadget, decltype(comp)> sg2(comp);

    sg2.emplace(3);
    sg2.emplace(1);
    sg2.emplace(2);
    for (auto& el : sg2)
        cout << el.id() << ", " << endl;
    cout << "----------------------------" << endl;
    return 0;
}

