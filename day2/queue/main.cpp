#include <iostream>
#include <queue>
#include <bitset>
#include <string>

using namespace std;

int main()
{
    cout << "Hello World!" << endl;
    queue<int> q;
    for (int i = 0 ; i < 10 ; ++i)
        q.push(i);

    while (!q.empty())
    {
        cout << q.front() << ", ";
        q.pop();
    }
    cout << endl;

    cout << " ----- bitset ----" << endl;
    bitset<8> bs(string("10111111"));
    cout << bs.to_string() << endl;
    cout << bs.to_ulong() << endl;
}


