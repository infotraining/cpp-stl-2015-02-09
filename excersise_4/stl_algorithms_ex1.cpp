#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <functional>
#include <cmath>
#include <numeric>

using namespace std;

class RandGen
{
    int range_;
public:
    RandGen(int range) : range_(range)
    {
    }

    int operator()() const
    {
        return rand() % range_;
    }
};

struct Avg
{
    double avg;
    int counter;
    int max_val;
    Avg() : avg(0), counter(0) {}
    operator()(int n)
    {
        counter++;
        avg += n;
        max_val = max(n, max_val);
    }

    double get_avg()
    {
        return avg/counter;
    }
};

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

int main()
{
    vector<int> vec(25);

    generate(vec.begin(), vec.end(), RandGen(30));

    print(vec);
    // 1a - wyświetl parzyste
    vector<int> even;
    copy_if(vec.begin(), vec.end(),
            back_insert_iterator<vector<int>>(even),
            [](int a) { return a % 2 == 0;});
    print(even);
    even.clear();

    for(auto& el : vec)
    {
        if ( el % 2 == 0)
            even.push_back(el);
    }
    print(even);

    // 1b - wyswietl ile jest nieparzystych
    cout << "odd = " << count_if(vec.begin(), vec.end(),
                                 [] (int a) { return a % 2 != 0;}) << endl;


    // 3 - tranformacja: podnieś liczby do kwadratu
    transform(vec.begin(), vec.end(), vec.begin(),
              [] (int a) { return a*a;});
    print(vec);

    // 4 - wypisz 5 najwiekszych liczb
    nth_element(vec.begin(), next(vec.begin(), 5),
                vec.end(), greater<int>());
    copy_n(vec.begin(), 5, ostream_iterator<int>(cout, ", "));
    cout << endl;

    // 5 - policz wartosc srednia
    double sum{};
    for_each(vec.begin(), vec.end(), [&sum] (int a) { sum+=a; });
    cout << "Avg = " << sum/vec.size() << endl;

    Avg avg = for_each(vec.begin(), vec.end(), Avg());
    cout << avg.get_avg() << " max = " << avg.max_val << endl;

    // 7 - utwórz dwa kontenery - 1. z liczbami mniejszymi lub równymi średniej,
    //        2. z liczbami większymi od średniej
    vector<int> greater_avg;
    vector<int> less_avg;
    partition_copy(vec.begin(), vec.end(),
                   back_inserter(greater_avg),
                   back_inserter(less_avg),
                   [&avg](int a) { return a > avg.get_avg(); });

    print(greater_avg);
    print(less_avg);

}
