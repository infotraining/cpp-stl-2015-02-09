## STL in  C++

### Additonal information

#### login and password for VM:

```
dev  /  tymczasowe
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

#### GIT

```
git clone https://bitbucket.org/infotraining/...
```

[git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)
