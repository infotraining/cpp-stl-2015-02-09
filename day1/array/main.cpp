#include <iostream>
#include <string>
#include <string.h>
#include <array>
#include <tuple>

using namespace std;

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}

int main()
{
    cout << "std array" << endl;

    std::array<int, 5> arr;

    // fill
    for (int i = 0 ; i < 5 ; ++i)
    {
        arr[i] = i;
    }

    for (int& elem : arr) // C++11 "for each" loop (range-based loop)
    {
        cout << elem << endl;
    }

    cout << "***********************" << endl;

    std::array<int, 3> small_arr{1,2,3};

    for(int& el : small_arr)
        cout << el << endl;

    // accessing element beyond array is possible (undefined behaviour)
    cout << "Small array fourth element: " << small_arr[3]<< endl;

    // with checking:
    try{
        cout << "Small array fourth element: " << small_arr.at(3)<< endl;
    }
    catch(const std::out_of_range& exc)
    {
        cout << exc.what() << endl;
    }


    print(arr);

    for(auto it = std::begin(small_arr) ; it != std::end(small_arr) ; ++it)
    {
        int& el = *it;
        cout << el  << endl;
    }

    // array interface

    array<char, 100> buff{}; // filled with zeros
    array<char, 100> buff2;  // uninitalized...

    strcpy(buff.data(), "ala ma kota"); // using in C functions

    for (auto& el : buff)
        cout << el;
    cout << endl;

    cout << buff.data() << endl;

    std::array<string, 3> three_strings{"ala", "ma", "kota"};

    cout << "second element: " << three_strings[1] << endl;
    cout << "second element (via tuple): " << std::get<1>(three_strings) << endl;

    // short tuple introduction...
    std::tuple<int, string, double> my_tuple{1, "leszek", 3.14};
    cout << "Pi = " << std::get<2>(my_tuple) << endl;

    return 0;
}

