#include <iostream>
#include <vector>

using namespace std;

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_(id)
    {
        cout << "ctor " << id_ << endl;
    }
    // -- automatic... //
    ~Gadget()
    {
        cout << "dtor" << endl;
    }

    Gadget(const Gadget& other) = delete;
//    {
//        id_ = other.id_;
//        cout << "cpy ctor " << id_ << endl;
//    }

    Gadget operator=(const Gadget& other) = delete;
//    {
//        id_ = other.id_;
//        cout << "cpy = " << endl;
//        return *this;
//    }
//    // move semantic C++11 and up
    Gadget(Gadget&& rother) noexcept
    {
        swap(id_, rother.id_);
        cout << "move ctor " << id_ << endl;
    }

    Gadget operator=(Gadget&& rother) noexcept
    {
        swap(id_, rother.id_);
        cout << "move =" << endl;
        return move(*this);
    }
};

int main()
{
    cout << "Hello vector!" << endl;

    vector<Gadget> vec;
    //vec.reserve(4);
    Gadget g(0);
    vec.emplace_back(move(g));
    vec.push_back(Gadget(0));  //move constructor !!
    vec.emplace_back(1);
    vec.emplace_back(2);
    cout << "gagdet 3" << endl;
    vec.emplace_back(3);
    cout << "-------" << endl;
    return 0;
}

