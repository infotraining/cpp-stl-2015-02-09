#include <iostream>
#include <vector>
#include <array>

using namespace std;

//template<class A>
//A max(const A& one, const A& two)
//{
//    return one;
//}

template <typename Cont>
void print(Cont c)
{
    cout << "[ ";
    for (const auto& el : c)
        cout << el << ", ";
    cout << " ]" << endl;
}


int main()
{
    cout << "Vector" << endl;

    // creating vector

    vector<int> v1(10);
    print(v1);

    vector<int> v2(10, -1);
    print(v2);

    // C++11 initializer list
    vector<int> v3{1,2,3,4};
    print(v3);

    vector<int> v{15, 2};  // two elements, instead of 15

    v.push_back(10);
    v.push_back(20);
    v.push_back(30);
    print(v);

    std::array<double, 3> arr{1.5, 1.6, 1.7};
    vector<double> v_cpy(arr.begin(), arr.end());
    print(v_cpy);

    // resizing vector

    //simple

    vector<int> v_1(10);
    v_1.push_back(10);
    print(v_1);
    cout << "cap " << v_1.capacity() << " size " << v_1.size() << endl;

    vector<int> v_2;
    v_2.resize(10);
    v_2.push_back(10);
    print(v_2);
    cout << "cap " << v_2.capacity() << " size " << v_2.size() << endl;

    vector<int> v_3;
    v_3.reserve(10);  // saves realocation time
    v_3.push_back(10);
    v_3.push_back(20);
    v_3.push_back(30);
    print(v_3);
    cout << "cap " << v_3.capacity() << " size " << v_3.size() << endl;

    // C++98 style
    for (vector<int>::const_iterator it = v_3.begin() ;
         it != v_3.end() ; ++it)
    {
        cout << *it << endl;
    }

    // C++11 style
    for(auto it = begin(v_3) ; it!= end(v_3) ; ++it)
    {
        cout << *it << endl;  // non-const iterator
    }

    cout << "--------------" << endl;

    //for(auto it = end(v_3)-1 ; it!= begin(v_3)-1 ; --it) // wrong...
    for(auto it = v_3.rbegin() ; it!= v_3.rend() ; ++it)   // reverse_it
    {
        cout << *it << ", ";  // non-const iterator
    }
    cout << endl;

    cout << "--------------" << endl;

    for(auto it = v_3.cbegin() ; it != v_3.cend() ; ++it)
    {
        cout << *it << endl;  // const iterator
    }

    cout << "size of vector: " << v.size() << endl;
    cout << "capacity: " << v.capacity() << endl;

    vector<int> test_size;//
    test_size.reserve(10000);

    size_t curr_capacity = 0;
    for (int i = 0 ; i < 10000 ; ++i)
    {
        test_size.push_back(1);
        if (curr_capacity != test_size.capacity())
        {
            cout << test_size.capacity() << " " << test_size.size()  << endl;
        }
        curr_capacity = test_size.capacity();
    }

    cout << "----------------" << endl;
    test_size.push_back(1);
    cout << "size: " << test_size.size() << endl;
    cout << "cap: " << test_size.capacity() << endl;

    //tricky, c++98 version
    //vector<int>(test_size.begin(), test_size.end()).swap(test_size);

    test_size.shrink_to_fit(); // C++11 only
    cout << "size: " << test_size.size() << endl;
    cout << "cap: " << test_size.capacity() << endl;

    return 0;
}


